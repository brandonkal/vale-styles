# Vale Styles

This repository contains a comprehensive collection of styles for linting English. It draws from the style guides of large companies, governments, and great writers. This tool focuses primarily on providing a unified style with smart rules. Rules are sorted by source, but they are designed to function as a single unit. See the example config file usage.

The primary goal of this ruleset is to never report a false positive. This means that some suggestions, such as preferring sentence case for headings have been removed. You should be able to run this on any piece of writing and never find the suggestions to not improve your prose. This makes the tool useful for CI integration.

## What is linting

In software programming, a linter is a tool that statically analyzes source code to flag bugs, stylelistic errors, and suspicious constructs. In software programming, linting is the first line of defense for catching bugs such as typos before actually running the code and test suite.

With natural language processing, these styles check to ensure consistant and clear writing style for prose. These checks are useful for writing blog posts, marketing materials, and technical documentation.

<!-- vale off -->

### Examples of errors

- The computer sees a new device. (Anthromorphism)
- He took an SAT test. (Redundant)
- Conflict is increasibly innevitable. (Uncomparable)
- A chip off the old block. (Cliche)

<!-- vale on -->

See the YAML files here to review the other checks included.

### Workflow

This is not a general-purpose writing aid. It doesn't teach you how to write. Specifically, Vale focuses on writing style rather than grammatical correctness. As an author, you should continue to employ the tools you are already using such as spell checkers, ProWritingAid, Microsoft Word's grammar checker, or Grammarly. Those tools will offer suggestions that may be correct or irrelevant. After drafting a document, send the results through Vale. Vale will ensure consistency across multiple authors.

Use these styles to maintain consistent and error-free writing. You should be able to use these styles as a failing step in your CI system. If you find a case where the suggestion does not improve the document, please file a pull request removing the rule with a note of the failing snippet in question.

### Why

Computers are much better at tedious tasks than humans. This tool will find issues in writing first, allowing the human editors more time to focus on more important tasks such as guiding content. I wanted some sane but exhaustive defaults, and so this collection of rules was compiled.

### Levels

This ruleset uses the levels "suggestion", "warning", and "error" to signify the sureness of the flag.
This means that rules marked as an "error" are not necessarily more important than a "suggestion". Some rules, such as the oxford comma, are listed as a suggestion because the linter cannot guarantee that the flagged sentence is incorrect. As an author, you should eliminate all errors flagged by Vale. You or your proofreader should also review and consider all warnings and suggestions to ensure the style guide is followed. Often, implementings the suggestions and warnings will improve the quality of writing. However, because english is not a science, sometimes human judgement and understanding of context will determine if a lint message should be ignored. As an author, do not be concerned if some warnings and suggestions still exist when publishing.

An example of a suggestion that should be ignored:

American English styleguides insist that commas should be placed inside quotation marks. However, in technical writing, this often reduces clarity. As such, punctuation should not be placed into quotation marks for literal values:

> **Incorrect**: The default administrator password is "admin."<br/> **Correct**: The default administrator password is "admin".

### Usage

This repository contains style guides. To run Vale, you need to download the Vale binary and run it on your files. It works best on markdown and HTML files, but you can also use it against code files to lint your english in comments. Only a few spelling tests are included. Prefer to use a dedicated tool such as cspell for checking spelling.

## License

[MIT](https://choosealicense.com/licenses/mit/)

Copyright © 2019 Brandon Kalinowski (brandonkal)<br />
Copyright © 2018 TestTheDocs
